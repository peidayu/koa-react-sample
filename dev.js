const webpack = require("webpack");
const koaWebpack = require("koa-webpack");
const webpackConfig = require("./webpack.config.js");

const compiler = webpack(webpackConfig);

async function dev(app) {
  const middleware = await koaWebpack({
    compiler,
    devMiddleware: {
      publicPath: "/dist",
    },
    hotClient: {},
  });
  app.use(middleware);
  app.use(require("koa-webpack-hot-middleware")(compiler));
}

module.exports = { dev };
