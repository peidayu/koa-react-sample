const Koa = require("koa");
const path = require("path");
const { dev } = require("./dev");

const app = new Koa();

(async () => {
  if (process.env.mode === "development") {
    await dev(app);
  }
  app.use(require("koa-static")(path.join(__dirname, "./views")));
  app.listen(8801);
})();
