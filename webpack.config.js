const webpack = require("webpack");
const path = require("path");

const config = {
  entry: ["react-hot-loader/patch", "./src/index.js"],
  output: {
    path: path.resolve(__dirname, "./views/dist"),
    filename: "bundle.js",
  },
  module: {
    rules: [
      {
        test: /\.(js|jsx)$/,
        use: "babel-loader",
        include: [
          path.resolve(__dirname, "./node_modules/@antv"),
          path.resolve(__dirname, "./src"),
        ],
      },
    ],
  },
  mode: "development",
  devServer: {
    static: {
      directory: "./dist",
    },
  },
};

module.exports = config;
